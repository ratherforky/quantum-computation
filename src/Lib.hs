{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
module Lib
    ( someFunc, toV4, toM44, rowColProd
    , show3sf
    , pretty, prettyWithFactor, ket
    , prettyPrintWithFactor, prettyPrint
    ) where

import Data.Complex
import Linear.Matrix
import Linear.V4
import Linear.Metric
import Text.Printf (printf)

toV4 :: [a] -> V4 a
toV4 [w,x,y,z] = V4 w x y z
toV4 _ = error "Wrong dimensionality"

toM44 :: [[a]] -> M44 a
toM44 = toV4 . map toV4

rowColProd :: Num a => V4 a -> V4 a -> a
rowColProd u v = dot u v

tonc :: M44 Double
tonc = V4 (V4 1 0 0 0)
          (V4 0 0 0 1)
          (V4 0 0 1 0)
          (V4 0 1 0 0)

zh :: M44 Double
zh = V4 (V4 1 1   0  0)
        (V4 1 (-1)  0  0)
        (V4 0 0  (-1) (-1))
        (V4 0 0  (-1)  1)

zh' :: M44 Double
zh' = toM44 [[ 1,  1,  0,  0]
            ,[ 1, -1,  0,  0]
            ,[ 0,  0, -1, -1]
            ,[ 0,  0, -1,  1]
            ]

type Frac = (Int, Int)

class Pretty a where
  pretty :: a -> String

prettyPrint :: (Pretty a, Fractional a) => M44 a -> IO ()
prettyPrint = putStr . pretty

prettyPrintWithFactor :: Frac -> M44 Double -> IO ()
prettyPrintWithFactor fac = putStr . prettyWithFactor fac

instance (Pretty a, Fractional a) => Pretty (M44 a) where
  pretty = pretty' Nothing 7

instance Pretty Double where
  pretty = show3sf

instance Pretty a => Pretty (Complex a) where
  pretty (r :+ c) = concat [pretty r, "+", pretty c, "i"]

prettyWithFactor :: Frac -> M44 Double -> String
prettyWithFactor frac = pretty' (Just frac) 7

pretty' :: (Pretty a, Fractional a) => Maybe Frac -> Int -> M44 a -> String
pretty' factor padding m44 = unlines $ map concat
  [[     "    ┏                            ┓"]
  ,[ mNom,  " ┃",     a,    b,    c,    d,"┃"]
  ,[ mLine, " ┃",     e,    f,    g,    h,"┃"]
  ,[ mDenom," ┃",     i,    j,    k,    l,"┃"]
  ,[     "    ┃",     m,    n,    o,    p,"┃"]
  ,[     "    ┗                            ┛"]
  ]
  where
    (mNom,mLine,mDenom) = fromMaybeFrac factor
    factorForMatrix = toDouble factor
    
    V4 (V4 a b c d)
       (V4 e f g h)
       (V4 i j k l)
       (V4 m n o p) = fmap (fmap (padded padding . pretty)) (m44 !!/ factorForMatrix)

fromMaybeFrac :: Maybe Frac -> (String, String, String)
fromMaybeFrac Nothing = ("   ","   ","   ")
fromMaybeFrac (Just (numerator, denominator)) = (padShow 3 numerator, "───", padShow 3 denominator)

toDouble :: Fractional a => Maybe Frac -> a
toDouble Nothing = 1
toDouble (Just (x,y)) = fromIntegral x / fromIntegral y

padShow :: Show a => Int -> a -> String
padShow n = padded n . show

show3sf :: Double -> String
show3sf d
  | d == fromIntegral (truncate d) = printf "%.0g" d
  | otherwise                      = error "Not integral"

padded :: Int -> String -> String
padded n s
  | spaces < 0 = error "String too long to show"
  | otherwise  = replicate spaces ' ' ++ s
  where
    spaces = n - length s

ket :: V4 Double -> String
ket (V4 w x y z) = concat [ show3sf w, "|00⟩", " + "
                          , show3sf x, "|01⟩", " + "
                          , show3sf y, "|10⟩", " + "
                          , show3sf z, "|11⟩"
                          ]

someFunc :: IO ()
someFunc = putStrLn "someFunc"

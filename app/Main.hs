module Main where

import Lib

import Linear.Matrix
import Linear.V4

uF :: M44 Double
uF = toM44 
  [[ 1,  0,  0,  0]
  ,[ 0,  1,  0,  0]
  ,[ 0,  0, -1,  0]
  ,[ 0,  0,  0,  1]
  ]

u0 :: M44 Double
u0 = toM44 
  [[-1,  0,  0,  0]
  ,[ 0,  1,  0,  0]
  ,[ 0,  0,  1,  0]
  ,[ 0,  0,  0,  1]
  ]

hth :: M44 Double
hth = (1/2) *!! toM44 
  [[ 1,  1,  1,  1]
  ,[ 1, -1,  1, -1]
  ,[ 1,  1, -1, -1]
  ,[ 1, -1, -1,  1]
  ]

d :: M44 Double
d = (-1) *!! hth !*! u0 !*! hth

factorisedD :: M44 Double
factorisedD = d !!/ (1/2)

q1 :: IO ()
q1 = do
  putStrLn "a) Matrix for oracle operation Uf for marked element X0 = 10:"
  prettyPrint uF

  putStrLn "b) Matrix D:"
  prettyPrintWithFactor (1,2) d

  let oneStep = d !*! uF
      g = oneStep !*! hth

  putStr $ unlines 
    [ "c) One step of Grover's algorithm:"
    , "   G = ((D)(Uf))^T(H⊗H) "
    , "   T = 1 for N = 4 so"
    , "   G = (D)(Uf)(H⊗H) ="
    , pretty g
    , "   Apply G to |00⟩:"
    , "     " ++ (ket $ g !* (V4 1 0 0 0))
    , "     = |10⟩"
    , ""
    ]

  let extraStep = oneStep !*! oneStep !*! hth
      finalState = extraStep !* (V4 1 0 0 0)
      alphaMarked = (V4 0 0 1 0) `rowColProd` finalState

  putStr $ unlines
    [ "d) G if T = 2:"
    , "   G = (D)(Uf)(D)(Uf)(H⊗H) ="
    , pretty extraStep
    , "   Final state if T = 2:"
    , "     " ++ ket finalState
    , ""
    , concat [ "   So Prob(|10⟩) = |", show3sf alphaMarked, "|²"
             , " = 1/4"]
    ]


main :: IO ()
main = q1
